package blatt3.visualwrapper;

import java.util.Arrays;

import org.eclipse.jdt.annotation.NonNull;

/**
 * Dokumentation siehe Interface ArrayWrapper
 * 
 * @author Petra Becker-Pechau
 * @version WI-AD WiSe 2016
 *
 */
class ArrayWrapperImpl implements ArrayWrapper
{
	private int @NonNull [] _array;
	protected int _startIndex;
	private int _length;

	/**
	 * Initialisiert einen neuen ArrayWrapper mit dem gegebenen Array (das Array
	 * wird nicht kopiert, sondern direkt als Referenzvariable verwendet)
	 * 
	 * @require array != null
	 * @require array.length > 0
	 */
	public ArrayWrapperImpl(int @NonNull [] array)
	{
		_array = array;
		_startIndex = 0;
		_length = array.length;
	}

	/**
	 * Initialisiert einen neuen Unterabschnitt. Dokumentation siehe
	 * getSubAbschnitt()
	 * 
	 * @require abschnitt != null
	 * @require startIndex <= endIndexRelativ
	 * @require startIndexRelativ >= 0
	 * @require startIndexRelativ < abschnitt.getLength()
	 * @require endIndexRelativ >= 0
	 * @require endIndexRelativ < abschnitt.getLength()
	 */
	protected ArrayWrapperImpl(@NonNull ArrayWrapperImpl abschnitt, int startIndexRelativ, int endIndexRelativ)
	{
		_array = abschnitt._array;
		_startIndex = abschnitt._startIndex + startIndexRelativ;
		_length = (endIndexRelativ - startIndexRelativ) + 1;
	}

	@Override public @NonNull ArrayWrapper getSubAbschnitt(int startIndexRelativ, int endIndexRelativ)
	{
		return new ArrayWrapperImpl(this, startIndexRelativ, endIndexRelativ);
	}

	@Override public int get(int index)
	{
		return (_array[index + _startIndex]);
	}

	@Override public int getLast()
	{
		return get(getLastIndex());
	}

	@Override public int getLastIndex()
	{
		return getLength() - 1;
	}

	@Override public int getLength()
	{
		return _length;
	}

	@Override public void swap(int i1, int i2)
	{
		assert i1 >= 0 : "i1 >= 0";
		assert i1 < getLength() : "i1 < getLength()";
		assert i2 >= 0 : "i2 >= 0";
		assert i2 < getLength() : "i2 < getLength()";

		int tmp = _array[i1 + _startIndex];
		_array[i1 + _startIndex] = _array[i2 + _startIndex];
		_array[i2 + _startIndex] = tmp;
	}

	@Override public void set(int index, int value)
	{
		assert index >= 0 : "index >= 0";
		assert index < getLength() : "index < getLength()";

		_array[index + _startIndex] = value;
	}

	@Override public @NonNull String toString()
	{
		return "Start: " + _startIndex + " Länge: " + _length + " " + abschnittToString() + Arrays.toString(_array);
	}

	private @NonNull String abschnittToString()
	{
		String ergebnis = "[";
		for (int i = _startIndex; i < getLength() + 1 + _startIndex; i++)
		{
			ergebnis += _array[i] + ",";
		}
		if (getLength() > 0)
		{
			ergebnis += _array[getLength() - 1];
		}
		ergebnis += "] ";
		return ergebnis;

	}

	@Override public int @NonNull [] getCopy()
	{
		int[] result = new int[getLength()];
		for (int i = 0; i < result.length; i++)
		{
			result[i] = _array[i + _startIndex];
		}
		return result;
	}
}