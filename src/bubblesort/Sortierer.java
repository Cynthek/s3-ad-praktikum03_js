package bubblesort;

/**
 * 
 * @author Julia-Marie, Chris Diese Klasse sortiert int-Werte nach dem
 *         Bubblesort-Prinzip in aufsteigender Reihenfolge
 *
 */
public class Sortierer
{
    /**
     * Das Array wird n mal von Anfang bis Ende durchlaufen. Dabei wird immer
     * der Inhalt der aktuellen Array-Zelle mit dem Inhalt der nachfolgenden
     * Zelle verglichen. Ist der Inhalt der nachfolgenden Zelle kleiner, werden
     * die Inhalte vertauscht.
     */
    public static void sort(int[] array)
    {
	// Er durchläuft das ganze Array so oft, wie das Array Plätze hat.
	for (int x = 0; x < array.length; x++)
	{
	    // Er tauscht EINE Zahl von ganz links nach ganz rechts (wenn nötig)
	    for (int i = 0, j = 1; j < array.length; i++, j++)
	    {

		if (array[i] > array[j])
		{
		    int temp = array[i];
		    array[i] = array[j];
		    array[j] = temp;
		}
	    }
	}
    }

}
