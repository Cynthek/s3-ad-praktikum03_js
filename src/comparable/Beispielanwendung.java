package comparable;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Julia-Marie, Chris
 *
 */
public class Beispielanwendung
{
    private static ArrayList<Person> unsortiert;
    ArrayList<Person> sortiert;

    public static void main(String[] args)
    {
	unsortiert = erstelleListe();
	druckeListe(unsortiert);
	System.out.println("\n");
	unsortiert.sort(null);
	druckeListe(unsortiert);
	PersonenAlterComparator vergleicher=new PersonenAlterComparator();
	unsortiert.sort(vergleicher);
	System.out.println("\n");
	druckeListe(unsortiert);

    }

    private static ArrayList<Person> erstelleListe()
    {
	ArrayList<Person> _liste = new ArrayList<>();
	_liste.add(new Person("Karl", 12));
	_liste.add(new Person("Claudia", 34));
	_liste.add(new Person("Xaver", 42));
	_liste.add(new Person("Peter", 12));
	_liste.add(new Person("Karl", 14));
	_liste.add(new Person("Anja", 21));
	_liste.add(new Person("Waldtraud", 77));
	_liste.add(new Person("Anke", 21));
	_liste.add(new Person("Christian", 23));
	_liste.add(new Person("Julia", 22));
	_liste.add(new Person("Rüdiger", 64));
	
	
	return _liste;
    }

    private static void druckeListe(ArrayList<Person> liste)
    {
	for (int i = 0; i < liste.size(); i++)
	{
	    System.out.println(liste.get(i).toString());
	}
    }

}
