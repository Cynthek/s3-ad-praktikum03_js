package bubblesort;

import blatt3.sort.ArrayCreator;

public class Beispielclient
{
   
    public static void main(String[] args)
    {
	 int[] beispiel = ArrayCreator.createGemischt(5);
	 for (int i=0; i<beispiel.length; i++)
	 {
	     System.out.println(beispiel[i]); 
	 }
	 System.out.println("\n");
	 Sortierer.sort(beispiel);
	 for (int i=0; i<beispiel.length; i++)
	 {
	     System.out.println(beispiel[i]); 
	 }
    }
    
}
