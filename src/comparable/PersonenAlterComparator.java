package comparable;
import java.util.Comparator;

public class PersonenAlterComparator implements Comparator<Person>
{

    @Override
    /**
     * Vergleicht zwei Personen anhand ihres Alters.
     * @return 0 Das Alter ist gleich.
     * @return -1 Die zweite Person ist älter als die erste.
     * @return 1 Die erste Person ist älter als die zweite.
     */
    public int compare(Person person1, Person person2)
    {	
	int result=0;
	if (person1.getAlter()==person2.getAlter())
	{
	    result= 0;
	}
	if (person1.getAlter()<person2.getAlter())
	{
	   result= -1;
	}
	if(person1.getAlter()>person2.getAlter())
	{
	    result= 1;
	}
	return result;
    }

}
