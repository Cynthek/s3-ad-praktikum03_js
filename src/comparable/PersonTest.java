package comparable;

import static org.junit.Assert.*;

import org.junit.Test;

public class PersonTest
{

    @Test
    public void testPerson()
    {
	
    }

    @Test
    public void testEqualsObject()
    {
	Person karl=new Person ("Karl",12);
	Person karl2= new Person("Karl",12);
	assertEquals(karl, karl2);
	
	Person Eva=new Person("Eva",23);
	Person eva2=new Person("Eva",22);
	assertFalse(Eva.equals(eva2));
    }

    @Test
    public void testCompareTo()
    {
	Person adamJung = new Person("Adam",12);
	Person adam2=new Person("Adam",12);
	assertEquals(0, adamJung.compareTo(adam2));
	
	Person adamAlt= new Person("Adam",23);
	assertEquals(-1, adamJung.compareTo(adamAlt));
	assertEquals(1, adamAlt.compareTo(adamJung));
	
	Person boris= new Person("Boris",23);
	assertEquals(-1, adamJung.compareTo(boris));
	assertEquals(1, boris.compareTo(adamJung));
    }
    @Test
    public void testCompareStrings()
    {
	Person karl =new Person("Karl", 12);
	
	String eins= "Anna";
	String zwei ="Alexandra";
	
	int result =karl.compareStrings(eins, zwei);
	assertEquals(2, result);
	
	String drei = "Anja";
	String vier= "Anke";
	result = karl.compareStrings(drei, vier);
	assertEquals(1, result);
	
	String fünf= "Xaver";
	String sechs = "Boris";
	result = karl.compareStrings(fünf, sechs);
	assertEquals(2, result);
	
    }
    

}
