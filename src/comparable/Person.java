package comparable;

/**
 * Diese Klasse repräsentiert Personen mit Alter und Namen. Zwei Personen lassen
 * sich anhand von Namen und Alter vergleichen.
 * 
 * @author Julia, Chris
 *
 */

public class Person implements Comparable<Person>
{
    private String _name;
    private int _alter;

    public Person(String name, int alter)
    {
	_name = name;
	_alter = alter;
    }

    @Override
    /**
     * Prüft auf Gleichheit, nicht auf Identität.
     */
    public boolean equals(Object object)
    {
	boolean result = true;
	if (object instanceof Person)
	{
	    Person o = (Person) object;
	    if (o._alter != this._alter)
	    {
		result = false;
	    }
	    // The result is true if and only if the argument is not
	    // null and is a String object
	    // that represents the same sequence of characters as this object.
	    if (!o._name.equals(this._name))
	    {
		result = false;
	    }
	}
	return result;
    }

    @Override
    /**
     * Diese Methode ordnet zwei Personen nach ihrem Namen, oder wenn dieser
     * gleich ist, nach ihrem Alter.
     * 
     * @param vergleichsperson
     *            Die Person, mit der verglichen werden soll.
     * @return 0, wenn Name und Alter gleich sind,
     * @return -1 , wenn der Name der zu vergleichenden Person später im
     *         Alphabet vorkommt,
     * @return 1, wenn der Name der zu vergleichenden Person früher im Alphabet
     *         kommt
     */
    public int compareTo(Person vergleichsperson)
    {
	int result= 0; 
	//Wenn Name und Alter gleich sind:
	if (this.equals(vergleichsperson))
	{
	    return 0;
	}
	//Wenn Name gleich ist, nach Alter sortieren
	if (this._name.equals(vergleichsperson._name))
	{
	    if (this._alter<vergleichsperson._alter)
	    {
		return -1;
	    }
	    else 
	    {
		return 1;
	    }
	}
	// Name vergleichen, Wenn Vergleichsperson später im Telefonbuch vorkommt:
	else  if(compareStrings(_name, vergleichsperson._name)== 1);
	{
	   result = -1;
	}
	//Wenn Person früher im Telefonbuch vorkommt
	if (compareStrings(_name, vergleichsperson._name)==2)
	{
	    result =1;
	}
	return result;
    }
    @Override
    public String toString()
    {
	return "Name: " + getName() + ", Alter: " + getAlter();
    }
    public int getAlter()
    {
	return _alter;
    }
    public String getName()
    {
	return _name;
    }

    /**
     * Diese Hilfsmethode vergleicht zwei Strings darauf, welcher früher im
     * Alphabet vorkommt.
     * Diese Methode sollte nicht aufgerufen werden, wenn die beiden Strings gleich sind.
     * @param eins,
     *            zwei Die zu vergleichenden Strings
     * @return 1, wenn der erste String früher im Alphabet vorkommt,
     * @return 2, wenn der zweite String zuerst vorkommt.
     * @require !eins.equals(zwei)
     */
    public int compareStrings(String eins, String zwei)
    {
	assert !eins.equals(zwei) : "Vorbedingung verletzt: !eins.equals(zwei)";
	int schleife = Math.min(eins.length(), zwei.length());
	eins= eins.toLowerCase();
	zwei= zwei.toLowerCase();
	int i = 0;
	int result=0;
	while (i < schleife)
	{
	    //chars in ints umwandeln, um vergleichen zu können
	    int x = Character.getNumericValue(eins.charAt(i));
	    int y = Character.getNumericValue(zwei.charAt(i));
	    if (x < y)
	    {
		return 1;
	    }
	    if (y < x)
	    {
		return 2;
	    }
	    if (x == y)
	    {
		i++;
	    }
	}
	return result;
    }
}
