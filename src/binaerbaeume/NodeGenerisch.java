package binaerbaeume;

public class NodeGenerisch<E>
{
    private E _object;
    private NodeGenerisch<E> _leftChild;
    private NodeGenerisch<E> _rightChild;

    public NodeGenerisch(E object)
    {
	_object = object;
    }

    public NodeGenerisch(E object, NodeGenerisch<E> leftChild)
    {
	_object = object;
	_leftChild = leftChild;
    }

    public NodeGenerisch(E object, NodeGenerisch<E> leftChild,
	    NodeGenerisch<E> rightChild)
    {
	_object = object;
	_leftChild = leftChild;
	_rightChild = rightChild;
    }

    public E getValue()
    {
	return _object;
    }

    /**
     * Gibt die Anzahl aller Kindknoten inkl. sich selbst zurück.
     * 
     * @return Anzahl aller Kindknoten
     */
    public int getNodeCount()
    {
	int result = 1;
	if (_leftChild != null)
	{
	    result = result + _leftChild.getNodeCount();
	}
	if (_rightChild != null)
	{
	    result = result + _rightChild.getNodeCount();
	}
	return result;
    }

    /**
     * Liefert den linken Kindknoten.
     * 
     * @return der linke Kindknoten
     */
    public NodeGenerisch<E> getLeftChild()
    {
	return _leftChild;
    }

    /**
     * Liefert den rechten Kindknoten.
     * 
     * @return der rechte Kindknoten
     */
    public NodeGenerisch<E> getRightChild()
    {
	return _rightChild;
    }
}
